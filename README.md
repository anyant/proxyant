# ProxyAnt

### 打包

```bash
./build.sh
```

### 配置

[example/config.toml](example/config.toml)

### Server

- `0.0.0.0:80`: HTTP端口，自动跳转到HTTPS
- `0.0.0.0:443`: HTTPS端口，路径 `/proxyant-ws` 分流到 V2ray
- `127.0.0.1:2333`: V2ray WebSocket端口

```bash
docker rm -f proxyant-server
docker run -d \
    --name proxyant-server \
    --restart unless-stopped \
    --log-driver json-file --log-opt max-size=50m --log-opt max-file=10 \
    -v "$(pwd)/config.toml":/srv/config.toml \
    -v "$(pwd)/config":/config \
    -v "$(pwd)/data":/data \
    --network host \
    guyskk/proxyant:latest /usr/bin/proxyant server
```

### Sidecar

- `0.0.0.0:1080`: HTTP Proxy端口
- `0.0.0.0:1085`: Socks5 Proxy端口
- `0.0.0.0:1090`: Shadowsocks端口

```bash
docker rm -f proxyant-sidecar
docker run -d \
    --name proxyant-sidecar \
    --restart unless-stopped \
    --log-driver json-file --log-opt max-size=50m --log-opt max-file=10 \
    -p 127.0.0.1:1080:1080 \
    -p 127.0.0.1:1085:1085 \
    -p 127.0.0.1:1090:1090 \
    -v "$(pwd)/config.toml":/srv/config.toml \
    guyskk/proxyant:latest /usr/bin/proxyant sidecar

# sidecar support multiple config files
# proxyant sidecar <CONFIG_FILE>,<CONFIG_FILE>,...
```

### 验收

```python
import requests
proxies = {
    'http': 'http://127.0.0.1:1080',
    'https': 'http://127.0.0.1:1085',
}
response = requests.get('https://httpbin.org/ip', proxies=proxies)
print(response.json())
```