# STATE: base image
FROM guyskk/proxyant-build:caddy as base
RUN apk add --no-cache bash logrotate curl tar util-linux
ARG TZ="Asia/Shanghai"
ENV TZ ${TZ}
RUN ln -sf /usr/share/zoneinfo/${TZ} /etc/localtime

# STATE: prepare forego
FROM base as forego
COPY ./tmp/forego.tgz /tmp/forego.tgz
RUN cd /usr/bin && tar -xzf /tmp/forego.tgz \
    && chmod +x /usr/bin/forego \
    && rm /tmp/forego.tgz && \
    /usr/bin/forego version

# STAGE: prepare v2ray
FROM base as v2ray
COPY ./tmp/v2ray.zip /tmp/v2ray.zip
RUN mkdir -p /tmp/v2ray \
    && unzip /tmp/v2ray.zip -d /tmp/v2ray/ \
    && mv /tmp/v2ray/v2ray /usr/bin \
    && mv /tmp/v2ray/v2ctl /usr/bin \
    && mv /tmp/v2ray/geoip.dat /usr/bin/geoip.dat \
    && mv /tmp/v2ray/geosite.dat /usr/bin/geosite.dat \
    && chmod +x /usr/bin/v2ray \
    && chmod +x /usr/bin/v2ctl \
    && rm -rf /tmp/v2ray /var/cache/apk/* \
    && /usr/bin/v2ray -version

# STATE: prepare proxyant
FROM guyskk/proxyant-build:golang as build
WORKDIR /go/src/app
ARG GOPROXY=""
RUN go env -w GO111MODULE=on && \
    go env -w GOPROXY=${GOPROXY}
COPY ["./go.*", "./"]
RUN go mod download
COPY ["./internal", "./internal"]
RUN go build -o proxyant internal/cmd/proxyant.go \
    && chmod u+x proxyant \
    && ./proxyant --help

# STAGE: final image
FROM base
COPY --from=forego ["/usr/bin/forego", "/usr/bin/"]
COPY --from=v2ray [ "/usr/bin/v2ray", "/usr/bin/v2ctl", "/usr/bin/geoip.dat", "/usr/bin/geosite.dat", "/usr/bin/"]
COPY --from=build ["/go/src/app/proxyant", "/usr/bin/"]
ARG FOREGO_VERSION
ARG V2RAY_VERSION
ARG CADDY_VERSION
ARG GO_VERSION
ARG PROXYANT_COMMIT_ID
ENV FOREGO_VERSION=${FOREGO_VERSION} V2RAY_VERSION=${V2RAY_VERSION} CADDY_VERSION=${CADDY_VERSION} GO_VERSION=${GO_VERSION} PROXYANT_COMMIT_ID=${PROXYANT_COMMIT_ID}
EXPOSE 80 443
CMD ["/usr/bin/proxyant"]
