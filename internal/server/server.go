package server

import (
	"anyant/proxyant/internal/common"
	"fmt"
	"log"
	"net/url"
	"os"
	"strings"
	"syscall"
)

type object = common.JSONObject
type array = common.JSONArray

var jsonDumps = common.JSONPrettyDumps
var envConfig = common.GetEnvConfig

func getV2rayWsConfig(config *common.Config) object {
	v2ray := config.V2ray
	return object{
		"listen":   "127.0.0.1",
		"port":     v2ray.WsPort,
		"tag":      "tunnel-ws",
		"protocol": "vmess",
		"settings": object{
			"clients": []object{
				{
					"id":      v2ray.Token,
					"alterId": 0,
				},
			},
		},
		"streamSettings": object{
			"network": "ws",
			"wsSettings": object{
				"path": v2ray.WsPath,
			},
		},
	}
}

func getV2rayVmessConfig(config *common.Config) object {
	v2ray := config.V2ray
	return object{
		"listen":   "0.0.0.0",
		"port":     v2ray.VmessPort,
		"tag":      "tunnel-vmess",
		"protocol": "vmess",
		"settings": object{
			"clients": []object{
				{
					"id":      v2ray.Token,
					"alterId": 0,
				},
			},
		},
	}
}

func getV2rayPortalInboundConfig(server *common.ServerConfig) object {
	listen, port := common.ParseHostPort(server.Portal)
	bridgeHost, bridgePort := common.ParseHostPort(server.Bridge)
	tag := fmt.Sprintf("inbound-portal-%s", server.Domain)
	return object{
		"listen":   listen,
		"port":     port,
		"tag":      tag,
		"protocol": "dokodemo-door",
		"settings": object{
			"address": bridgeHost,
			"port":    bridgePort,
			"network": "tcp",
		},
	}
}

func getV2rayPortalConfig(server *common.ServerConfig) object {
	tag := fmt.Sprintf("portal-%s", server.Domain)
	return object{
		"tag":    tag,
		"domain": server.Domain,
	}
}

func getV2rayPortalRuleList(server *common.ServerConfig) []object {
	inboundTag := fmt.Sprintf("inbound-portal-%s", server.Domain)
	portalTag := fmt.Sprintf("portal-%s", server.Domain)
	ruleDomain := fmt.Sprintf("full:%s", server.Domain)
	return []object{
		{
			"type":        "field",
			"inboundTag":  array{inboundTag},
			"outboundTag": portalTag,
		},
		{
			"type":        "field",
			"inboundTag":  array{"tunnel-ws"},
			"domain":      array{ruleDomain},
			"outboundTag": portalTag,
		},
	}
}

func generateV2rayConfig(config *common.Config) string {
	inbounds := []object{
		getV2rayWsConfig(config),
	}
	if config.V2ray.VmessPort >= 0 {
		inbounds = append(inbounds, getV2rayVmessConfig(config))
	}
	var portals []object
	var rules []object
	for _, server := range config.Servers {
		if server.Portal != "" {
			inbounds = append(inbounds, getV2rayPortalInboundConfig(&server))
			portals = append(portals, getV2rayPortalConfig(&server))
			rules = append(rules, getV2rayPortalRuleList(&server)...)
		}
	}
	result := object{
		"log":      object{"loglevel": config.V2ray.LogLevel},
		"inbounds": inbounds,
		"outbounds": []object{
			{
				"protocol": "freedom",
				"settings": object{},
			},
		},
		"reverse": object{
			"portals": portals,
		},
		"routing": object{
			"rules": rules,
		},
	}
	return jsonDumps(result)
}

func getRedirectRoute(server *common.ServerConfig) object {
	location := server.Redirect
	return object{
		"match": []object{
			{
				"host": []string{server.Domain},
			},
		},
		"handle": []object{
			{
				"handler":     "static_response",
				"status_code": 302,
				"headers": object{
					"Location": []string{location},
				},
			},
		},
		"terminal": false,
	}
}

func getStaticRoute() object {
	return object{
		"handle": []object{
			{
				"handler": "file_server",
				"root":    envConfig().CaddyHTMLPath,
			},
		},
		"terminal": true,
	}
}

func getForwardRoute(server *common.ServerConfig) object {
	var forward string = server.Forward
	if !strings.HasPrefix(forward, "http") {
		if strings.HasSuffix(forward, ":443") {
			forward = "https://" + forward
		} else {
			forward = "http://" + forward
		}
	}
	forwardUrl, err := url.Parse(forward)
	if err != nil {
		log.Fatalf("invalid forward url: %s", forward)
	}
	isHttps := forwardUrl.Scheme == "https"
	port := forwardUrl.Port()
	if port == "" {
		if isHttps {
			port = "443"
		} else {
			port = "80"
		}
	}
	dialAddress := fmt.Sprintf("%s:%s", forwardUrl.Hostname(), port)
	transport := object{
		"protocol": "http",
	}
	if isHttps {
		transport = object{
			"protocol": "http",
			"tls": object{
				"server_name": forwardUrl.Hostname(),
			},
		}
	}
	return object{
		"handle": []object{
			{
				"handler": "reverse_proxy",
				"upstreams": []object{
					{
						"dial": dialAddress,
					},
				},
				"transport": transport,
				"headers": object{
					"request": object{
						"set": object{
							"host": []string{forwardUrl.Host},
						},
					},
				},
			},
		},
		"terminal": true,
	}
}

func getV2rayWsRoute(v2ray *common.V2rayConfig) object {
	v2rayHandler := fmt.Sprintf("127.0.0.1:%d", v2ray.WsPort)
	return object{
		"match": []object{
			{
				"path": []string{v2ray.WsPath},
			},
		},
		"handle": []object{
			{
				"handler": "reverse_proxy",
				"upstreams": []object{
					{
						"dial": v2rayHandler,
					},
				},
				"transport": object{
					"protocol": "http",
					"versions": []string{"1.1", "2", "h2c"},
				},
			},
		},
		"terminal": true,
	}
}

func getServerRoute(server *common.ServerConfig, subroutes []object) object {
	return object{
		"match": []object{
			{
				"host": []string{server.Domain},
			},
		},
		"handle": []object{
			{
				"handler": "subroute",
				"routes":  subroutes,
			},
		},
		"terminal": false,
	}
}

func generateCaddyConfig(config *common.Config) string {
	v2rayWsRoutes := []object{getV2rayWsRoute(&config.V2ray)}
	var routes443 []object
	var routes80 []object
	var skipHTTPSDomains []string
	for _, server := range config.Servers {
		if server.Portal != "" {
			continue
		}
		if server.Redirect != "" {
			if server.AutomaticHTTPS {
				routes443 = append(routes443, getRedirectRoute(&server))
			} else {
				routes80 = append(routes80, getRedirectRoute(&server))
			}
		} else if server.Forward != "" {
			var subroutes []object
			if server.AutomaticHTTPS {
				subroutes = append(subroutes, v2rayWsRoutes...)
			}
			subroutes = append(subroutes, getForwardRoute(&server))
			if server.AutomaticHTTPS {
				routes443 = append(routes443, getServerRoute(&server, subroutes))
			} else {
				routes80 = append(routes80, getServerRoute(&server, subroutes))
			}
		} else {
			var subroutes []object
			if server.AutomaticHTTPS {
				subroutes = append(subroutes, v2rayWsRoutes...)
			}
			subroutes = append(subroutes, getStaticRoute())
			if server.AutomaticHTTPS {
				routes443 = append(routes443, getServerRoute(&server, subroutes))
			} else {
				routes80 = append(routes80, getServerRoute(&server, subroutes))
			}
		}
		if !server.AutomaticHTTPS {
			skipHTTPSDomains = append(skipHTTPSDomains, server.Domain)
		}
	}
	result := object{
		"admin": object{
			"disabled": true,
		},
		"apps": object{
			"tls": object{
				"automation": object{
					"policies": []object{
						{
							"issuers": []object{
								{
									"module": "acme",
									"email":  envConfig().CaddyAcmeEmail,
								},
							},
						},
					},
				},
			},
			"http": object{
				"servers": object{
					"gateway_443": object{
						"listen": []string{":443"},
						"routes": routes443,
						"logs":   object{},
					},
					"gateway_80": object{
						"listen": []string{":80"},
						"routes": routes80,
						"automatic_https": object{
							"skip": skipHTTPSDomains,
						},
						"logs": object{},
					},
				},
			},
		},
	}
	return jsonDumps(result)
}

func generateForegoConfig() string {
	caddy := fmt.Sprintf("caddy: %s run --config %s",
		envConfig().CaddyCommandPath, envConfig().CaddyConfigPath)
	v2ray := fmt.Sprintf("v2ray: %s -config %s",
		envConfig().V2rayCommandPath, envConfig().V2rayConfigPath)
	return strings.Join([]string{caddy + "\n", v2ray + "\n"}, "")
}

// Start start server
func Start(config *common.Config) {
	fmt.Printf("* generate %s\n", envConfig().V2rayConfigPath)
	v2rayConfig := generateV2rayConfig(config)
	common.WriteConfigFile(envConfig().V2rayConfigPath, v2rayConfig)

	fmt.Printf("* generate %s\n", envConfig().CaddyConfigPath)
	caddyConfig := generateCaddyConfig(config)
	os.MkdirAll(envConfig().CaddyHTMLPath, 0755)
	common.WriteConfigFile(envConfig().CaddyConfigPath, caddyConfig)

	fmt.Printf("* generate %s\n", envConfig().ForegoConfigPath)
	foregoConfig := generateForegoConfig()
	common.WriteConfigFile(envConfig().ForegoConfigPath, foregoConfig)

	fmt.Println("* start v2ray and caddy")
	foregoArgs := []string{envConfig().ForegoCommandPath, "start", "-f", envConfig().ForegoConfigPath}
	fmt.Println("> " + strings.Join(foregoArgs, " "))
	syscall.Exec(envConfig().ForegoCommandPath, foregoArgs, os.Environ())
}
