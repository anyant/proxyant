package common

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

// JSONObject json object
type JSONObject map[string]interface{}

// JSONArray json array
type JSONArray []interface{}

// JSONPrettyDumps dump json with indent
func JSONPrettyDumps(data JSONObject) string {
	text, err := json.MarshalIndent(data, "", "    ")
	if err != nil {
		log.Fatal(err)
	}
	return string(text)
}

// WriteConfigFile write config file
func WriteConfigFile(path string, text string) {
	var err error
	err = os.MkdirAll(filepath.Dir(path), 0755)
	if err != nil {
		log.Fatal(err)
	}
	err = ioutil.WriteFile(path, []byte(text), 0644)
	if err != nil {
		log.Fatal(err)
	}
}

// ParseHostPort parse host:port string
func ParseHostPort(value string) (string, uint16) {
	parts := strings.Split(value, ":")
	if len(parts) != 2 {
		log.Fatalf("invalid host port %v", value)
	}
	var host string = parts[0]
	port, err := strconv.ParseUint(parts[1], 10, 16)
	if err != nil {
		log.Fatalf("invalid host port %v", err)
	}
	return host, uint16(port)
}
