package common

import (
	"io/ioutil"

	toml "github.com/pelletier/go-toml"
)

// ServerConfig object
type ServerConfig struct {
	Domain         string
	Forward        string
	AutomaticHTTPS bool
	Redirect       string
	Portal         string
	Bridge         string
}

// V2rayConfig object
type V2rayConfig struct {
	LogLevel            string
	Token               string
	VmessPort           int16
	WsPort              int16
	WsPath              string
	ShadowsocksPassword string
}

// Config object
type Config struct {
	Servers []ServerConfig
	V2ray   V2rayConfig
}

func _portConfigOf(value interface{}) int16 {
	if value == nil {
		value = int64(-1)
	}
	return int16(value.(int64))
}

func _stringConfigOf(value interface{}) string {
	if value == nil {
		value = ""
	}
	return value.(string)
}

func _parseV2rayConfig(rawV2ray *toml.Tree) *V2rayConfig {
	vmessPort := _portConfigOf(rawV2ray.Get("vmess_port"))
	wsPath := _stringConfigOf(rawV2ray.Get("ws_path"))
	if wsPath == "" {
		wsPath = "/proxyant-ws"
	}
	wsPort := _portConfigOf(rawV2ray.Get("ws_port"))
	if wsPort < 0 {
		wsPort = 2333
	}
	token := _stringConfigOf(rawV2ray.Get("token"))
	logLevel := _stringConfigOf(rawV2ray.Get("log_level"))
	if logLevel == "" {
		logLevel = "info"
	}
	shadowsocksPassword := _stringConfigOf(rawV2ray.Get("shadowsocks_password"))
	return &V2rayConfig{
		LogLevel:            logLevel,
		Token:               token,
		VmessPort:           vmessPort,
		WsPort:              wsPort,
		WsPath:              wsPath,
		ShadowsocksPassword: shadowsocksPassword,
	}
}

func _parseServerConfig(rawServer *toml.Tree) *ServerConfig {
	automaticHTTPS := (rawServer.GetDefault("automatic_https", true)).(bool)
	return &ServerConfig{
		Domain:         _stringConfigOf(rawServer.Get("domain")),
		Forward:        _stringConfigOf(rawServer.Get("forward")),
		AutomaticHTTPS: automaticHTTPS,
		Redirect:       _stringConfigOf(rawServer.Get("redirect")),
		Portal:         _stringConfigOf(rawServer.Get("portal")),
		Bridge:         _stringConfigOf(rawServer.Get("bridge")),
	}
}

// LoadFileConfig load config from filepath
func LoadFileConfig(filepath string) (*Config, error) {
	data, err := ioutil.ReadFile(filepath)
	if err != nil {
		return nil, err
	}
	rawConfig, err := toml.Load(string(data))
	if err != nil {
		return nil, err
	}
	v2ray := *_parseV2rayConfig(rawConfig.Get("v2ray").(*toml.Tree))
	var servers []ServerConfig
	rawServers := rawConfig.GetArray("servers").([]*toml.Tree)
	for _, s := range rawServers {
		servers = append(servers, *_parseServerConfig(s))
	}
	config := Config{
		V2ray:   v2ray,
		Servers: servers,
	}
	return &config, nil
}
