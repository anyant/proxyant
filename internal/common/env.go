package common

import "os"

// EnvConfig object
type EnvConfig struct {
	ProcfilePath      string
	V2rayCommandPath  string
	V2rayConfigPath   string
	CaddyCommandPath  string
	CaddyConfigPath   string
	CaddyHTMLPath     string
	CaddyAcmeEmail    string
	ForegoCommandPath string
	ForegoConfigPath  string
}

func _getenv(key string, defaultValue string) string {
	value := os.Getenv(key)
	if value == "" {
		value = defaultValue
	}
	return value
}

func _createEnvConfig() *EnvConfig {
	return &EnvConfig{
		V2rayCommandPath:  _getenv("V2RAY_COMMAND_PATH", "/usr/bin/v2ray"),
		V2rayConfigPath:   _getenv("V2RAY_CONFIG_PATH", "/srv/v2ray/config.json"),
		CaddyCommandPath:  _getenv("CADDY_COMMAND_PATH", "/usr/bin/caddy"),
		CaddyConfigPath:   _getenv("CADDY_CONFIG_PATH", "/srv/caddy/config.json"),
		CaddyHTMLPath:     _getenv("CADDY_HTML_PATH", "/srv/caddy/html"),
		CaddyAcmeEmail:    _getenv("CADDY_ACME_EMAIL", ""),
		ForegoCommandPath: _getenv("FOREGO_COMMAND_PATH", "/usr/bin/forego"),
		ForegoConfigPath:  _getenv("FOREGO_CONFIG_PATH", "/srv/Procfile"),
	}
}

var _envConfigInstance *EnvConfig

// InitEnvConfig ...
func InitEnvConfig() {
	_envConfigInstance = _createEnvConfig()
}

// GetEnvConfig ...
func GetEnvConfig() *EnvConfig {
	return _envConfigInstance
}
