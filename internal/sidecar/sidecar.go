package sidecar

import (
	"anyant/proxyant/internal/common"
	"fmt"
	"os"
	"strings"
	"syscall"
)

type object = common.JSONObject
type array = common.JSONArray

var jsonDumps = common.JSONPrettyDumps
var envConfig = common.GetEnvConfig

func generateInbounds() []object {
	var inbounds []object
	inbounds = append(inbounds, object{
		"listen":   "0.0.0.0",
		"port":     1080,
		"tag":      "inbound-http",
		"protocol": "http",
		"settings": object{
			"accounts":         []object{},
			"allowTransparent": false,
		},
	})
	inbounds = append(inbounds, object{
		"listen":   "0.0.0.0",
		"port":     1085,
		"tag":      "inbound-socks",
		"protocol": "socks",
		"settings": object{
			"auth": "noauth",
			"udp":  false,
		},
	})
	return inbounds
}

func generateShadowsocksInbound(v2ray *common.V2rayConfig) object {
	return object{
		"port":     1090,
		"protocol": "shadowsocks",
		"settings": object{
			"method":   "aes-256-gcm",
			"password": v2ray.ShadowsocksPassword,
		},
	}
}

func getWsTag(server *common.ServerConfig) string {
	return fmt.Sprintf("vmess-ws-%s", server.Domain)
}

func generateWsOutbound(server *common.ServerConfig, v2ray *common.V2rayConfig) object {
	return object{
		"protocol": "vmess",
		"settings": object{
			"vnext": []object{
				{
					"address": server.Domain,
					"port":    443,
					"users": []object{
						{
							"id":      v2ray.Token,
							"alterId": 0,
						},
					},
				},
			},
		},
		"streamSettings": object{
			"network":  "ws",
			"security": "tls",
			"wsSettings": object{
				"path": v2ray.WsPath,
			},
		},
		"tag": getWsTag(server),
	}
}

func generateBridgeConfig(server *common.ServerConfig) object {
	tag := fmt.Sprintf("bridge-%s", server.Domain)
	return object{
		"tag":    tag,
		"domain": server.Domain,
	}
}

func generateBridgeOutboundConfig(server *common.ServerConfig) object {
	tag := fmt.Sprintf("outbound-bridge-%s", server.Domain)
	return object{
		"tag":      tag,
		"protocol": "freedom",
		"settings": object{
			"redirect": server.Bridge,
		},
	}
}

func generateBridgeRuleList(server *common.ServerConfig) []object {
	inboundTag := fmt.Sprintf("bridge-%s", server.Domain)
	outboundTag := fmt.Sprintf("outbound-bridge-%s", server.Domain)
	ruleDomain := fmt.Sprintf("full:%s", server.Domain)
	return []object{
		{
			// bridge 连接 portal 的路由
			"type":        "field",
			"inboundTag":  array{inboundTag},
			"domain":      array{ruleDomain},
			"balancerTag": "balancer",
		},
		{
			// portal 连接 bridge 的路由
			"type":        "field",
			"inboundTag":  array{inboundTag},
			"outboundTag": outboundTag,
		},
	}
}

func generateRoutingDefaultRules() []object {
	return []object{
		{
			"type": "field",
			"ip": array{
				"0.0.0.0/8",
				"10.0.0.0/8",
				"100.64.0.0/10",
				"127.0.0.0/8",
				"169.254.0.0/16",
				"172.16.0.0/12",
				"192.0.0.0/24",
				"192.0.2.0/24",
				"192.168.0.0/16",
				"198.18.0.0/15",
				"198.51.100.0/24",
				"203.0.113.0/24",
				"::1/128",
				"fc00::/7",
				"fe80::/10",
			},
			"outboundTag": "direct",
		},
		{
			"type":        "field",
			"outboundTag": "direct",
			"domain":      array{"geosite:cn"},
		},
		{
			"type":        "field",
			"ip":          array{"geoip:private"},
			"outboundTag": "direct",
		},
		{
			"type":        "field",
			"network":     "tcp",
			"balancerTag": "balancer",
		},
	}
}

func generateRoutingConfig(selector []string, rules []object) object {
	return object{
		"domainStrategy": "IPOnDemand",
		"balancers": []object{
			{
				"tag":      "balancer",
				"selector": selector,
			},
		},
		"rules": rules,
	}
}

func generateDNSConfig() object {
	return object{
		"servers": array{
			"1.1.1.1",
			object{ // aliyun
				"address": "223.5.5.5",
				"domains": array{"geosite:cn"},
			},
			object{ // dnspod
				"address": "119.29.29.29",
				"domains": array{"geosite:cn"},
			},
			"8.8.8.8",
			"localhost",
		},
	}
}

func generateV2rayConfig(configList []*common.Config) string {
	var inbounds []object = generateInbounds()
	for _, config := range configList {
		if config.V2ray.ShadowsocksPassword != "" {
			inbounds = append(inbounds, generateShadowsocksInbound(&config.V2ray))
			break
		}
	}
	var outbounds []object
	var tags []string
	var bridges []object
	var rules []object
	for _, config := range configList {
		for _, server := range config.Servers {
			if server.Redirect != "" {
				continue
			}
			if !server.AutomaticHTTPS {
				continue
			}
			if server.Bridge == "" {
				outbounds = append(outbounds, generateWsOutbound(&server, &config.V2ray))
				tags = append(tags, getWsTag(&server))
			} else {
				outbounds = append(outbounds, generateBridgeOutboundConfig(&server))
				bridges = append(bridges, generateBridgeConfig(&server))
				rules = append(rules, generateBridgeRuleList(&server)...)
			}
		}
	}
	rules = append(rules, generateRoutingDefaultRules()...)
	outbounds = append(outbounds, object{
		"protocol": "freedom",
		"settings": object{},
		"tag":      "direct",
	})
	outbounds = append(outbounds, object{
		"protocol": "blackhole",
		"settings": object{},
		"tag":      "blocked",
	})
	result := object{
		"log": object{
			"loglevel": configList[0].V2ray.LogLevel,
		},
		"reverse": object{
			"bridges": bridges,
		},
		"inbounds":  inbounds,
		"outbounds": outbounds,
		"routing":   generateRoutingConfig(tags, rules),
		"dns":       generateDNSConfig(),
	}
	return jsonDumps(result)
}

// Start start sidecar
func Start(configList []*common.Config) {
	fmt.Printf("* generate %s\n", envConfig().V2rayConfigPath)
	v2rayConfig := generateV2rayConfig(configList)
	common.WriteConfigFile(envConfig().V2rayConfigPath, v2rayConfig)

	fmt.Println("* start v2ray")
	v2rayArgs := []string{envConfig().V2rayCommandPath, "-config", envConfig().V2rayConfigPath}
	fmt.Println("> " + strings.Join(v2rayArgs, " "))
	syscall.Exec(envConfig().V2rayCommandPath, v2rayArgs, os.Environ())
}
