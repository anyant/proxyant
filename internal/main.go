package proxyant

import (
	"anyant/proxyant/internal/common"
	"anyant/proxyant/internal/server"
	"anyant/proxyant/internal/sidecar"
	"fmt"
	"os"
	"strings"
)

const _usage string = "Usage: proxyant server|sidecar <CONFIG_FILE>"

func loadConfigList(configPathList []string) ([]*common.Config, error) {
	var configList []*common.Config
	for _, p := range configPathList {
		config, err := common.LoadFileConfig(p)
		if err != nil {
			return nil, err
		}
		configList = append(configList, config)
	}
	return configList, nil
}

// Main entrypoint
func Main() {
	if !(2 <= len(os.Args) && len(os.Args) <= 3) {
		fmt.Println(_usage)
		return
	}
	command := os.Args[1]
	if command != "server" && command != "sidecar" {
		fmt.Println(_usage)
		return
	}
	isServer := command == "server"
	var configPath string = "config.toml"
	if len(os.Args) == 3 {
		configPath = os.Args[2]
	}
	configPathList := strings.Split(configPath, ",")
	if isServer && len(configPathList) > 1 {
		fmt.Println("Error: server not support multiple config files")
		return
	}
	configList, err := loadConfigList(configPathList)
	if err != nil {
		fmt.Println(err)
		return
	}
	common.InitEnvConfig()
	if isServer {
		server.Start(configList[0])
	} else {
		sidecar.Start(configList)
	}
}
