#!/bin/bash

set -ex

mkdir -p ./tmp

FOREGO_VERSION="ekMN3bCZFUn"
if [[ ! -e "tmp/forego.tgz" ]]; then
# https://github.com/ddollar/forego/issues/127
# curl --fail -Lo tmp/forego.tgz "https://bin.equinox.io/c/${FOREGO_VERSION}/forego-stable-linux-amd64.tgz"
curl --fail -Lo tmp/forego.tgz "https://ezfuns-pkg.oss-cn-zhangjiakou.aliyuncs.com/bin/forego-${FOREGO_VERSION}.tgz"
fi

V2RAY_VERSION="4.44.0"
if [[ ! -e "tmp/v2ray.zip" ]]; then
curl --fail -Lo tmp/v2ray.zip "https://github.com/v2fly/v2ray-core/releases/download/v${V2RAY_VERSION}/v2ray-linux-64.zip"
fi

CADDY_VERSION="2.4.2"
GO_VERSION="1.17.6"
PROXYANT_COMMIT_ID=$(git rev-parse --verify HEAD)

# https://stackoverflow.com/questions/68520864/how-to-disable-loading-metadata-while-executing-docker-build/70483395#70483395
function pullImageTag() {
    target=guyskk/proxyant-build:"$1"
    if [[ $(docker image inspect "$target" --format='OK') != 'OK' ]];then
        docker pull --platform linux/amd64 "$1:$2"
        docker tag "$1:$2" "$target"
    fi
}
pullImageTag caddy ${CADDY_VERSION}-alpine
pullImageTag golang ${GO_VERSION}

# shellcheck disable=SC2068
docker build \
    --platform linux/amd64 \
    --progress plain \
    --build-arg FOREGO_VERSION="$FOREGO_VERSION" \
    --build-arg V2RAY_VERSION="$V2RAY_VERSION" \
    --build-arg CADDY_VERSION="$CADDY_VERSION" \
    --build-arg GO_VERSION="$GO_VERSION" \
    --build-arg PROXYANT_COMMIT_ID="$PROXYANT_COMMIT_ID" \
    $@ -t guyskk/proxyant:latest .
